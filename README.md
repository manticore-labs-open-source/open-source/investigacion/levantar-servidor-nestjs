# Levantar servidor nestjs

Nestjs es un framework para backend muy similar a angular o como le describen en su pagina web "Un framework de Node.js progresivo para crear aplicaciones de servidor eficientes, confiables y escalables"

## Instalación
Para instalar nestjs necesitamos tener instalado Node JS.

Luego debemos instalar el cliente de nest con el siguiente comando.
```
npm i -g @nestjs/cli
```
ó si ya tenemos un proyecto npm podemos utilizar
```
npm i --save @nestjs/core @nestjs/common rxjs reflect-metadata
```
## Preparación
Para crear un nuevo proyecto en nestjs utilizaremos el comando 

```
nest new nombre-app
```
de esta forma:
![comando nest new](./imagenes/nestnew.png)

A continuación nos pedirá datos del proyecto y en el gestor paquetes que deseamos usar marcaremos npm.

![](./imagenes/nestjscreacion.png)
Luego de elegir el paquetes esperamos a que termine la creación del archivo.

al final tendremos una pantalla como la siguiente:
![](imagenes/nestjsfin.png)

el directorio se verá de la siguiente manera:
![](imagenes/directorio.png)

no se olviden de añadir un gitignore ya que los proyectos de nest no traen incluidos un gitignore.

En el repositorio añadiré un gitignore para node.

En la carpeta src dentro del proyecto podemos encontrar el archivo main ts donde se encuentra declarado el puerto y la forma en la que se levantará el servidor

![](./imagenes/maints.png)

Como se puede observar el código es muy parecido a levantar un servidor express

## Levantar el servidor

Para levantar el servidor nos ubicamos en la carpeta del proyecto en mi caso *ejemplo-mc*
![](./imagenes/directiorio2.png)

y aquí corremos el comando
```
npm run start
```
y vemos que el servidor está corriendo.
![](./imagenes/serveron.png)

Si no modificamos el archivo anterior el servidor debería estar corriendo en el puerto 3000.

![](./imagenes/test.png)

y eso es todo ya tenemos nuestro servidor corriendo.
<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>